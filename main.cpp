#include <iostream>
#include <vector>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

#define MAX_WIDTH 512
#define MAX_HEIGHT 512


void read_file(const char *file_path, unsigned char *input_buffer, int width, int height) {
    FILE *input_image = std::fopen(file_path, "rb");
    unsigned int bytes_loaded = fread(input_buffer, 1, width * height, input_image);

    if (bytes_loaded != width * height) {
        if (feof(input_image)) {
            printf("End of file!\n");
        }

        if (ferror(input_image)) {
            printf("Error reading from file.\n");
        }

        printf("Could not read from stream enough bytes.\n");
        printf("Expected %u, loaded %u bytes\n", width * height, bytes_loaded);
        exit(1);
    }

    fclose(input_image);
}

void correct_fisheye(const unsigned char *input_buffer, unsigned char *output_buffer, int width, int height) {
    double strength = 4;
    double correction_radius = sqrt(pow(height, 2) + pow(width, 2)) / strength;

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; j++) {
            int new_x = i - 256;
            int new_y = j - 256;
            double distance = sqrt(pow(new_y, 2) + pow(new_x, 2));
            double theta = 1;
            double r = distance / correction_radius;
            if (r != 0.0)
                theta = atan(r) / r;

            int source_x = (int) (256 + theta * new_x);
            int source_y = (int) (256 + theta * new_y);

            output_buffer[i + j * height] = input_buffer[source_x + source_y * height];
        }
    }
}

double get_SRN(const cv::Mat &output_jpeg) {
    cv::Scalar image_mean, std_dev;
    cv::meanStdDev(output_jpeg, image_mean, std_dev);
    return image_mean(0) / std_dev(0);
}


int main() {
    const int width = MAX_WIDTH;
    const int height = MAX_HEIGHT;

    auto *input_buffer = (unsigned char *) new char *[sizeof(unsigned char) * width * height];
    auto *output_buffer = (unsigned char *) new char *[sizeof(unsigned char) * width * height];

    //read file
    read_file("../fisheye_512x512_u8.raw", input_buffer, width, height);

    // fisheye correction
    correct_fisheye(input_buffer, output_buffer, width, height);

    cv::Mat output_img = cv::Mat(height, width, CV_8U, output_buffer).clone();
    if (!output_img.data) {
        perror("Failed to load image.");
        exit(1);
    }

    // JPEG compression
    cv::imwrite("../output.jpeg", output_img);

    // SRN of compressed image
    std::cout << get_SRN(output_img);

    free(input_buffer);

    return 0;
}